<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCardRepository")
 */
class ShoppingCard
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="shoppingCard", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineShoppingBag", mappedBy="shoppingBag", cascade={"persist"})
     */
    private $lineShoppingBags;

    public function __construct()
    {
        $this->lineShoppingBags = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|LineShoppingBag[]
     */
    public function getLineShoppingBags(): Collection
    {
        return $this->lineShoppingBags;
    }

    public function addLineShoppingBag(LineShoppingBag $lineShoppingBag): self
    {
        if (!$this->lineShoppingBags->contains($lineShoppingBag)) {
            $this->lineShoppingBags[] = $lineShoppingBag;
            $lineShoppingBag->setShoppingBag($this);
        }

        return $this;
    }

    public function removeLineShoppingBag(LineShoppingBag $lineShoppingBag): self
    {
        if ($this->lineShoppingBags->contains($lineShoppingBag)) {
            $this->lineShoppingBags->removeElement($lineShoppingBag);
            // set the owning side to null (unless already changed)
            if ($lineShoppingBag->getShoppingBag() === $this) {
                $lineShoppingBag->setShoppingBag(null);
            }
        }

        return $this;
    }
}
