<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="Products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LineShoppingBag", mappedBy="Product")
     */
    private $lineShoppingBags;

    public function __construct()
    {
        $this->lineShoppingBags = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|LineShoppingBag[]
     */
    public function getLineShoppingBags(): Collection
    {
        return $this->lineShoppingBags;
    }

    public function addLineShoppingBag(LineShoppingBag $lineShoppingBag): self
    {
        if (!$this->lineShoppingBags->contains($lineShoppingBag)) {
            $this->lineShoppingBags[] = $lineShoppingBag;
            $lineShoppingBag->setProduct($this);
        }

        return $this;
    }

    public function removeLineShoppingBag(LineShoppingBag $lineShoppingBag): self
    {
        if ($this->lineShoppingBags->contains($lineShoppingBag)) {
            $this->lineShoppingBags->removeElement($lineShoppingBag);
            // set the owning side to null (unless already changed)
            if ($lineShoppingBag->getProduct() === $this) {
                $lineShoppingBag->setProduct(null);
            }
        }

        return $this;
    }
}
