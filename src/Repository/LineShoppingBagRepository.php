<?php

namespace App\Repository;

use App\Entity\LineShoppingBag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LineShoppingBag|null find($id, $lockMode = null, $lockVersion = null)
 * @method LineShoppingBag|null findOneBy(array $criteria, array $orderBy = null)
 * @method LineShoppingBag[]    findAll()
 * @method LineShoppingBag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LineShoppingBagRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LineShoppingBag::class);
    }

//    /**
//     * @return LineShoppingBag[] Returns an array of LineShoppingBag objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LineShoppingBag
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
