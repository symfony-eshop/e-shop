<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\CategoryRepository;

class BaseController extends Controller
{
    /**
     * @Route("/base", name="base")
     */
    public function index(CategoryRepository $repo)
    {
        $categories = $repo->findAll();

        return $this->render('base/index.html.twig', 
           array('categories' => $categories));
        
    }

    
}
