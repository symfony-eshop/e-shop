<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ProductFormType;
use App\Entity\Product;
use App\Service\FileUploader;
use App\Repository\ProductRepository;


class ProductController extends Controller
{
    /**
     * @Route("/admin/create-product", name="create_product")
     * @Route("/admin/{id}/edit-product", name="edit_product")
     */
    public function index(Product $product = null, Request $request, FileUploader $fileUploader){
        
        if(!$product) {
            $product = new Product();
        }

        $form = $this->createForm(ProductFormType::class, $product);
         $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $file = $product->getImage();
            $fileName = $fileUploader->upload($file);
            $product->setImage($fileName);


            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('home', ["id" => $product->getId()]);
        }
        return $this->render('product/index.html.twig', [
            'form' => $form->createView(),
            'editMode' => $product->getId() ==! null]);
        
    }


      /**
     *  @Route("/product/{id}", name="show_product")
     */
    public function show(Product $product, $id, Request $request){
        if ($request->isMethod('POST')) {

            $number = $request->get("number");
    
            return $this->redirectToRoute("product_line", ["id" => $product->getId(), "number" => $number]);
        }

        return $this->render("product/show.html.twig", ["product" => $product, "imageURI" => $this->getParameter('brochures_URI')]);
    }

    /**
    *  @Route("/admin/{id}/remove-product", name="remove_product")
    */
    public function remove(Product $product) {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute("home", []);
    }



}
