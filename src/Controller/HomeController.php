<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;
use App\Repository\ProductRepository;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(ProductRepository $repo) {
       
        $products = $repo->findAll();

        return $this->render('home/index.html.twig', [
          "products" => $products,
         "imageURI" => $this->getParameter('brochures_URI')
        ]);
    }
}
