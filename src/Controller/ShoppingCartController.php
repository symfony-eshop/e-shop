<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\ShoppingCard;
use Doctrine\Common\Persistence\ObjectManager;

class ShoppingCartController extends Controller
{
   /**
     * @Route("/user/shopping-cart", name="shopping_cart")
     */
    public function index(Session $session = null, ObjectManager $manager)
    {
            if(!$session) {
            $session = new Session();
            }
            $session->start();

            $cart = $session->get("cart");
            if(!$cart){
                $cart = new ShoppingCard();
            }

            $totalPrice = 0;
            foreach($cart->getLineShoppingBags() as $line) {
                $totalPrice = $totalPrice + $line->getPrice();
            }
            
        

            $cart = $this->getDoctrine()->getManager()->merge($cart);
            $productLine = $cart->getLineShoppingBags();

            $session->set("cart", $cart);
            

            $manager->persist($cart);
            $manager->flush();
            
        return $this->render('shopping_cart/index.html.twig', [
            "productLine" => $productLine,
            "totalPrice" => $totalPrice
        ]);
    }

}
